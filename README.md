# Introduction 
This is a common library for cardholders domain services

# Build
- ./gradlew clean build

# Build without generating new soap sources
- ./gradlew clean build -x wsdl2java

# Build and install locally
- ./gradlew clean install -x wsdl2java

# Release
- commit
- push
- ./gradlew clean release -x wsdl2java
- insert version (empty for default)
- just confirm when prompted