package br.com.alelo.digitalservices.commons.test

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.web.servlet.MockMvc

@AutoConfigureMockMvc
abstract class BaseControllerTest : BaseTest() {

    @Autowired
    lateinit var mockMvc: MockMvc
}