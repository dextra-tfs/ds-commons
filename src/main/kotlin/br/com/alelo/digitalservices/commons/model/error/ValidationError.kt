package br.com.alelo.digitalservices.commons.model.error

data class ValidationError(val field: String, val message: String?)