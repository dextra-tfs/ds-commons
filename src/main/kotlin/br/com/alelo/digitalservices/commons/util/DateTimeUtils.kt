package br.com.alelo.digitalservices.commons.util

import java.time.ZoneId

class DateTimeUtils {
    companion object {
        fun defaultZoneId(): ZoneId = ZoneId.of("America/Sao_Paulo")
    }
}