package br.com.alelo.digitalservices.commons.util

import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec
import org.apache.commons.codec.binary.Base64

object CryptographyUtils {

    const val PRIVATE_KEY_HEADER = "-----BEGIN RSA PRIVATE KEY-----"
    const val PRIVATE_KEY_FOOTER = "-----END RSA PRIVATE KEY-----"
    const val PUBLIC_KEY_HEADER = "-----BEGIN PUBLIC KEY-----"
    const val PUBLIC_KEY_FOOTER = "-----END PUBLIC KEY-----"

    @JvmStatic
    fun aesEncrypt(content: String, secretKey: String, urlSafe: Boolean = true) =
        AES256.encrypt(content, secretKey, urlSafe)

    @JvmStatic
    fun aesDecrypt(content: String, secretKey: String, urlSafe: Boolean = true) =
        AES256.decrypt(content, secretKey, urlSafe)

    @JvmStatic
    fun removePrivatePemHeaders(privateKeyContent: String): String {
        return privateKeyContent
            .replace("\\n".toRegex(), "")
            .replace(PRIVATE_KEY_HEADER, "")
            .replace(PRIVATE_KEY_FOOTER, "")
    }

    @JvmStatic
    fun removePublicPemHeaders(privateKeyContent: String): String {
        return privateKeyContent
            .replace("\\n".toRegex(), "")
            .replace(PUBLIC_KEY_HEADER, "")
            .replace(PUBLIC_KEY_FOOTER, "")
    }
}

private object AES256 {
    private fun cipher(mode: Int, secretKey: String): Cipher {
        val c = Cipher.getInstance("AES/CBC/PKCS5Padding")
        val sk = SecretKeySpec(secretKey.toByteArray(Charsets.UTF_8), "AES")
        val iv = IvParameterSpec(secretKey.toByteArray(Charsets.UTF_8))
        c.init(mode, sk, iv)
        return c
    }

    fun encrypt(str: String, secretKey: String, urlSafe: Boolean): String {
        val encrypted =
            cipher(Cipher.ENCRYPT_MODE, secretKey)
                .doFinal(str.toByteArray(Charsets.UTF_8))
        return Base64(urlSafe).encodeAsString(encrypted).trim { it <= ' ' }
    }

    fun decrypt(str: String, secretKey: String, urlSafe: Boolean): String {
        val byteStr = Base64(urlSafe).decode(str.toByteArray(Charsets.UTF_8))
        return String(cipher(Cipher.DECRYPT_MODE, secretKey).doFinal(byteStr)).trim { it <= ' ' }
    }
}
