package br.com.alelo.digitalservices.commons.util

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class CryptographyUtilsTest {

    @Test
    fun `GIVEN a plan, WHEN encrypt the input, THEN input is encrypted`() {
        val encrypted = CryptographyUtils.aesEncrypt(PLAN_INPUT, AES_VALUE, true)
        Assertions.assertThat(encrypted).isNotBlank()
        Assertions.assertThat(encrypted).isEqualTo(ENCRYPTED_INPUT)
    }

    @Test
    fun `GIVEN a encrypted value, WHEN decrypt the input, THEN input is plan`() {
        val dec = CryptographyUtils.aesDecrypt(ENCRYPTED_INPUT, AES_VALUE, true)
        Assertions.assertThat(dec).isEqualTo(PLAN_INPUT)
    }

    @Test
    fun `GIVEN a document, WHEN encrypt with a worng AES key, THEN returns null`() {
        val cardNumber = "1234123412341234"

        Assertions.assertThatThrownBy {
            CryptographyUtils.aesEncrypt(cardNumber, "INVALD", true)
        }.isInstanceOf(java.security.InvalidAlgorithmParameterException::class.java)
    }

    @Test
    fun `GIVEN pem private key, WHEN remove headers, THEN returns null`() {
        val cleanKey = CryptographyUtils.removePrivatePemHeaders(PRIVATE_KEY_SAMPLE)
        Assertions.assertThat(cleanKey).doesNotContain(CryptographyUtils.PRIVATE_KEY_HEADER)
        Assertions.assertThat(cleanKey).doesNotContain(CryptographyUtils.PRIVATE_KEY_FOOTER)
    }

    @Test
    fun `GIVEN pem public key, WHEN remove headers, THEN returns null`() {
        val cleanKey = CryptographyUtils.removePublicPemHeaders(PUBLIC_KEY_SAMPLE)
        Assertions.assertThat(cleanKey).doesNotContain(CryptographyUtils.PUBLIC_KEY_HEADER)
        Assertions.assertThat(cleanKey).doesNotContain(CryptographyUtils.PUBLIC_KEY_FOOTER)
    }

    companion object {
        const val AES_VALUE = "DF3AEB25ED93ADB5"
        const val PLAN_INPUT = "12345678900"
        const val ENCRYPTED_INPUT = "d8w9kUrB2UKQe-nKv7qoOQ"

        const val PRIVATE_KEY_SAMPLE = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIJKQIBAAKCAgEAv8i\n-----END RSA PRIVATE KEY-----"
        const val PUBLIC_KEY_SAMPLE = "-----BEGIN PUBLIC KEY-----\n" +
            "MIICIjANBgkqhkiG9==\n-----END PUBLIC KEY-----\n"
    }
}